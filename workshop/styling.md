# Styling

Có 2 các chính để style React component:

1. Inline styles với `style` prop
2. CSS thuần với `className` prop

**`style` prop:**

-   Trong HTML bạn sẽ phải truyền một chuỗi string của CSS

```html
<div style="margin-top: 20px; background-color: blue;"></div>
```

-   Trong React thì bạn sẽ truyền 1 đối tượng vào CSS:

```jsx
<div style={{ marginTop: 20, backgroundColor: "blue" }} />
```

Tương tự như:

```jsx
const myStyles = {marginTop: 20, backgroundColor: 'blue'}
<div style={myStyles} />
```

> 🦉 Lưu ý rằng tên của thuộc tính là `camelCase` chứ không phải `kebab-cased`.
>
> Và nó trúng khớp với `style` props của DOM nodes (tham khảo
> [`CSSStyleDeclaration`](https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration)
> object).

**`className` prop:**

Trong HTML để gáng class name vào element ta dùng
`class`. Trong React ta dùng `className` prop.

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI TẬP 1 **

Trong bài này chúng ta sẽ dùng cả 2 cách để styling React components

Cho các class CSS:

```css
.box {
	border: 1px solid #333;
	display: flex;
	flex-direction: column;
	justify-content: center;
	text-align: center;
}
.box--large {
	width: 270px;
	height: 270px;
}
.box--medium {
	width: 180px;
	height: 180px;
}
.box--small {
	width: 90px;
	height: 90px;
}
```

🐧 Bạn sẽ phải thêm className và style props để styling các thẻ div.

#### ** ĐÁP ÁN 1 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<style type="text/css">
		.box {
			border: 1px solid #333;
			display: flex;
			flex-direction: column;
			justify-content: center;
			text-align: center;
		}
		.box--large {
			width: 270px;
			height: 270px;
		}
		.box--medium {
			width: 180px;
			height: 180px;
		}
		.box--small {
			width: 90px;
			height: 90px;
		}
	</style>
	<script type="text/babel">
		const smallBox = (
			<div
				className="box box--small"
				style={{ fontStyle: "italic", backgroundColor: "lightblue" }}
			>
				small lightblue box
			</div>
		);
		const mediumBox = (
			<div
				className="box box--medium"
				style={{ fontStyle: "italic", backgroundColor: "pink" }}
			>
				medium pink box
			</div>
		);
		const largeBox = (
			<div
				className="box box--large"
				style={{ fontStyle: "italic", backgroundColor: "orange" }}
			>
				large orange box
			</div>
		);

		const App = (
			<div>
				{smallBox}
				{mediumBox}
				{largeBox}
			</div>
		);
		ReactDOM.render(App, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 2 **

### Tạo custom component

🐧 Tạo 1 custom `<Box />` component renders ra thẻ `div`, nhận toàn bộ
props and và merge `style` và `className` props với props của `<Box />`.

`<Box />` sẽ được sử dụng như sau:

```jsx
<Box className="box--small" style={{ backgroundColor: "lightblue" }}>
	small lightblue box
</Box>
```

`<Box />` mặc định đã có `box` className và style `fontStyle: 'italic'`.

#### ** ĐÁP ÁN 2 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<style type="text/css">
		.box {
			border: 1px solid #333;
			display: flex;
			flex-direction: column;
			justify-content: center;
			text-align: center;
		}
		.box--large {
			width: 270px;
			height: 270px;
		}
		.box--medium {
			width: 180px;
			height: 180px;
		}
		.box--small {
			width: 90px;
			height: 90px;
		}
	</style>
	<script type="text/babel">
		function Box({ style, className = "", ...otherProps }) {
			return (
				<div
					className={`box ${className}`}
					style={{ fontStyle: "italic", ...style }}
					{...otherProps}
				/>
			);
		}

		const App = (
			<div>
				<Box
					className="box--small"
					style={{ backgroundColor: "lightblue" }}
				>
					small lightblue box
				</Box>
				<Box
					className="box--medium"
					style={{ backgroundColor: "pink" }}
				>
					medium pink box
				</Box>
				<Box
					className="box--large"
					style={{ backgroundColor: "orange" }}
				>
					large orange box
				</Box>
				<Box>sizeless box</Box>
			</div>
		);
		ReactDOM.render(App, document.getElementById("root"));
	</script>
</body>
```

<!-- tabs:end -->

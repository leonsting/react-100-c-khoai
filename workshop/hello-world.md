# Basic JS - Make FE Great again

Cách hiển thị “Make FE Great again” trên trang bằng HTML:

```html
<html>
	<body>
		<div>Make FE Great again</div>
	</body>
</html>
```

Từ đoạn HTML này , trình duyệt sẽ sinh ra
[DOM (Document Object Model)](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction). Trình duyệt sẽ cung cấp các DOM API để chúng ta truy cập vào DOM node bằng Javascript, vì thế chúng ta có thể tương tác cập nhật nó.

```html
<html>
	<body>
		<div>Hello World</div>
		<script type="module">
			// code JavaScript
		</script>
	</body>
</html>
```

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI 1 **

Chúng ta sẽ sử dụng JavaScript để tạo nút `div` DOM có nội dung text là "Make FE Great again" và chèn nó vào `document`.

Truy cập vào DOM node của thẻ div root:

```javascript
const rootElement = document.getElementById("root");
```

> Điều quan trọng là phải có hiểu biết cơ bản về cách tạo và tương tác với các nút DOM bằng JavaScript vì nó sẽ giúp bạn hiểu cách React hoạt động tốt hơn. Vì vậy, trong bài tập này, chúng tôi thực sự sẽ không sử dụng React.

#### ** ĐÁP ÁN 1**

```html
<body>
	<div id="root"></div>
	<script type="module">
		const rootElement = document.getElementById("root");
		const element = document.createElement("div");
		element.textContent = "Make FE Great Again";
		element.className = "container";
		rootElement.append(element);
	</script>
</body>
```

#### ** BÀI 2 **

-   Tạo nút div root

Thay vì chúng ta có sẵn nút `root` trong HTML,bạn hãy tạo nó bằng JavaScript.

#### ** ĐÁP ÁN 2 **

```html
<body>
	<script type="module">
		const rootElement = document.createElement("div");
		rootElement.setAttribute("id", "root");
		document.body.append(rootElement);
		const element = document.createElement("div");
		element.textContent = "Make FE Great Again";
		element.className = "container";
		rootElement.append(element);
	</script>
</body>
```

<!-- tabs:end -->

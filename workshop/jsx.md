# Sử dụng JSX

JSX trực quan hơn API React và dễ hiểu hơn khi đọc code. Nó là cú pháp đặc biệt giống HTML được xây dựng dựa trên React APIs:

```jsx
const ui = <h1 id="greeting">Hey there</h1>;

// ↓ ↓ ↓ ↓ biên dịch thành ↓ ↓ ↓ ↓

const ui = React.createElement("h1", { id: "greeting", children: "Hey there" });
```

Bởi vì JSX thực sự không phải là JavaScript, bạn phải chuyển đổi nó bằng cách sử dụng một thứ gọi là trình biên dịch mã. [Babel](https://babeljs.io) là một trong những công cụ như vậy.

> 🦉 Nếu bạn muốn xem cách JSX được biên dịch sang JavaScript,
> [sử dụng tool online babel REPL](https://babeljs.io/repl#?builtIns=App&code_lz=MYewdgzgLgBArgSxgXhgHgCYIG4D40QAOAhmLgBICmANtSGgPRGm7rNkDqIATtRo-3wMseAFBA&presets=react&prettier=true).

> Nếu bạn có thể luyện tập cách suy nghĩ để xem JSX sẽ được biên dịch như thế nào, bạn sẽ đọc và sử dụng nó hiệu quả hơn RẤT NHIỀU!

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI 1 **

🐧 Chuyển nội dung của bài `React API` sang sử dụng JSX

Chúng ta sẽ thêm một thẻ script cho Babel

```html
<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
```

Và sau đó đổi type của thẻ script JavaScript của chúng ta để Babel để biên dịch nó.

```html
<script type="text/babel">
```

> Thông thường, bạn sẽ biên dịch tất cả code của mình tại lúc `build`trước khi gửi ứng dụng của mình tới trình duyệt, nhưng vì Babel được viết bằng JavaScript, chúng ta có thể chạy nó trong trình duyệt để biên dịch code của chúng ta một cách nhanh chóng và nên trong bài tập này chúng ta chỉ sử dụng trực tiêp.

#### ** ĐÁP ÁN 1 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		const element = <div className="container">Make FE Great Again</div>;
		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI 2 **

"Interpolation"(Phép nội suy) được định nghĩa là "việc chèn một thứ gì đó có bản chất khác vào một thứ khác."

Ví dụ:

```javascript
const greeting = "Sup";
const subject = "World";
const message = `${greeting} ${subject}`;
```

🐧 Hãy gán `className` (`"container"`) và
`children` (`"Make FE Great Again"`) vào biến và nội suy vào JSX.

```jsx
const className = "container";
const children = "Make FE Great Again";
const element = <div className="hmmm">how do I make this work?</div>;
```

> 🦉 Tham khảo thêm
> https://reactjs.org/docs/introducing-jsx.html
>
> -   https://reactjs.org/docs/introducing-jsx.html#embedding-expressions-in-jsx
> -   https://reactjs.org/docs/introducing-jsx.html#specifying-attributes-with-jsx

#### ** ĐÁP ÁN 2 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		const children = "Make FE Great Again";
		const className = "container";
		const element = <div className={className}>{children}</div>;
		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI 3 **

### spread props

🐧 Sử dụng tóan tử spread để set các props cho component

```jsx
const children = "Make FE Great Again";
const className = "container";
const props = { children, className };
const element = <div />; // Set props vào thẻ div?
```

Nếu chúng ta sử dụng React APIs thì nó sẽ như thế này:

```jsx
const element = React.createElement("div", props);
```

Hoặc như vầy:

```jsx
const element = React.createElement("div", { ...props });
```

Tham khảo https://reactjs.org/docs/jsx-in-depth.html#spread-attributes

#### ** ĐÁP ÁN 3 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		const children = "Hello World";
		const className = "container";
		const props = { children, className };
		const element = <div {...props} />;
		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

<!-- tabs:end -->

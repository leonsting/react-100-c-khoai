# React APIs

React thực chất cũng sử dụng cùng các DOM APIs mà bạn đã sử dụng khi tạo các DOM node ở bài tập trước.

> Như ở đây trong `source code` của `React`,
> [React sử dụng document.createElement](https://github.com/facebook/react/blob/48907797294340b6d5d8fecfbcf97edf0691888d/packages/react-dom/src/client/ReactDOMComponent.js#L416).

Một điều quan trọng cần biết về React là nó hỗ trợ nhiều nền tảng (ví dụ: native và web). Mỗi nền tảng này có code riêng cần thiết để tương tác với nền tảng đó.

Do đó, chúng ta cần phải có 2 thành phần React để xây dựng 1 ứng dụng web:

-   React: chịu trách nhiệm tạo các phần tử React (giống như
    `document.createElement()`)
-   ReactDOM: chịu trách nhiệm render các phần tử React sang DOM (giống như
    `rootElement.append()`)

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI 1 **

Chuyền cách làm của bài `Hello World` trước bằng cách dùng React API

Thông thường các bạn có thể sử dụng React và React DOM từ "package
registry" như [npm](https://npmjs.com) ([react](https://npm.im/react) và
[react-dom](https://npm.im/react-dom)). Nhưng đối với những bài này, chúng ta sẽ sử dụng các file script có sẵn trên [unpkg.com](https://unpkg.com) để không cần phải cài đặt chúng.

Sau khi thêm các file script, bạn sẽ có 2 biến global là:
`React` and `ReactDOM`.

Ví dụ về React API:

```javascript
const elementProps = { id: "element-id", children: "Hello world!" };
const elementType = "h1";
const reactElement = React.createElement(elementType, elementProps);
ReactDOM.render(reactElement, rootElement);
```

#### ** ĐÁP ÁN 1 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script type="module">
		const rootElement = document.getElementById("root");
		const element = React.createElement("div", {
			className: "container",
			children: "Make FE Great Again",
		});
		ReactDOM.render(element, rootElement);
	</script>
</body>
```

#### ** BÀI 2 **

### Lồng phần tử

Sử dụng JavaScript + React để hiển thị đoạn HTML sau:

```html
<body>
	<div id="root">
		<div class="container">
			<span>Make FE</span>
			<span>Great Again</span>
		</div>
	</div>
</body>
```

#### ** ĐÁP ÁN 2 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script type="module">
		const rootElement = document.getElementById("root");
		const element = React.createElement("div", {
			className: "container",
			children: [
				React.createElement("span", null, "Make FE"),
				" ",
				React.createElement("span", null, "Great Again"),
			],
		});
		ReactDOM.render(element, rootElement);
	</script>
</body>
```

<!-- tabs:end -->

# Tạo Custom Component

Giống như JavaScript thuần, chúng tao thường tạo các function để share và reuse. Đối với JSX, bạn cũng có thể làm như vậy. Trong React các function đó được gọi là "components" và chúng có vài thuộc tính đặc biệt.

Components là functions có trả về gía trị "renderable"
(như React elements, strings, `null`, numbers, ...)

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI TẬP 1 **

🐧 Ở bài này chúng ta sẽ muốn DOM generate ra như sau:

```html
<div className="container">
	<div className="message">Make FE</div>
	<div className="message">Great Again</div>
</div>
```

Trong trường hợp này, sẽ tốt hơn nếu chúng ta trành việc trùng lặp code bằng cách tạo React Element như sau :

```jsx
<div className="message">{children}</div>
```

Vì vậy chúng ta cần tạo một function nhận vào tham số là `children` và trả về React element. Khi đó chúng ta áp dụng phép nội suy để gọi function trong JSX.

```jsx
<div>{message({ children: "Make FE" })}</div>
```

> 🦉 Đây không phải là cách tốt nhất chúng ta nên viết custom React component. Chúng ta sẽ sử dụng các khác trong bài tập kế tiếp.
>
> Tham khảo https://reactjs.org/docs/jsx-in-depth.html

#### ** ĐÁP ÁN 1 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		// NOTE: đây không phải là cách tạo custom componnent.
		function message({ children }) {
			return <div className="message">{children}</div>;
		}

		const element = (
			<div className="container">
				{message({ children: "Hello World" })}
				{message({ children: "Goodbye World" })}
			</div>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 2 **

### Sử dụng React.createElement để tạo custom componnent

Ở các bài tập trước chúng ta sử dụng `React.createElement("tên thẻ")`, nhưng tham số đầu tiên của `React.createElement` cũng có thể là một fucntion trả về 1 các gì đó renderable.

🐧 Thay vì gọi function `message` , chúng ta gửi nó thông qua tham số đầu tiên của
`React.createElement` và gửi `{children: 'Hello World'}` như tham số thứ 2.

#### ** ĐÁP ÁN 2 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		function message({ children }) {
			return <div className="message">{children}</div>;
		}

		const element = (
			<div className="container">
				{React.createElement(message, { children: "Make FE" })}
				{React.createElement(message, { children: "Great Again" })}
			</div>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 3 **

### Sử dụng JSX để tạo custom componnent

Giống như cách sử dụng JSX để tạo thẻ `div` sẽ đơn giản hơn sử dụng `React.createElement` API, và cho custom components cũng vậy.
Nên nhớ rằng Babel chịu trách nhiệm cho việc nhận JSX của chúng ta và biên dịch sang `React.createElement` , vì vậy chúng ta cần có 1 cách để cho biết Babel cần phải biên dịch JSX gọi function của chúng ta .

Và đây là Babel đối chiều để biên dịch `JSX` sang `React.createElement`:

🐧 Hãy cho phép Babel biết cách biên dịch sang đúng Component `Message` của chúng ta:

```javascript
ui = <Capitalized /> // React.createElement(Capitalized)
ui = <property.access /> // React.createElement(property.access)
ui = <Property.Access /> // React.createElement(Property.Access)
ui = <Property['Access'] /> // SyntaxError
ui = <lowercase /> // React.createElement('lowercase')
ui = <kebab-case /> // React.createElement('kebab-case')
ui = <Upper-Kebab-Case /> // React.createElement('Upper-Kebab-Case')
ui = <Upper_Snake_Case /> // React.createElement(Upper_Snake_Case)
ui = <lower_snake_case /> // React.createElement('lower_snake_case')
```

#### ** ĐÁP ÁN 3 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		// Đây là một component 🎉
		function Message({ children }) {
			return <div className="message">{children}</div>;
		}

		const element = (
			<div className="container">
				<Message>Make FE</Message>
				<Message>Great Again</Message>
			</div>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 4 **

### Runtime validation với PropTypes

Sửa component `Message` như sau:

```javascript
function Message({ subject, greeting }) {
	return (
		<div className="message">
			{greeting}, {subject}
		</div>
	);
}
```

Và chúng ta sẽ sử dụng nó như sau:

```javascript
<Message greeting="Make" subject="FE" />
<Message greeting="Great" subject="Again" />
```

Chuyện gì sẽ xảy ra khi chúng ta quên truyền `greeting` hoặc `subject`? Nó không render props. Và chúng ta sẽ có 1 dấu phẩy lủng lẳng đâu đó. Chúng ta tốt hơn nên có 1 cách để validate giá trị props của component và thông báo đã truyền sai. Và React có 1 tính năng để làm việc này đó là `propTypes` :

```javascript
function FavoriteNumber({ favoriteNumber }) {
	return <div>My favorite number is: {favoriteNumber}</div>;
}

const PropTypes = {
	number(props, propName, componentName) {
		if (typeof props[propName] !== "number") {
			return new Error("Some useful error message here");
		}
	},
};

FavoriteNumber.propTypes = {
	favoriteNumber: PropTypes.number,
};
```

Và nếu chúng ta sử dụng `FavoriteNumber`:

```javascript
<FavoriteNumber favoriteNumber="not a number" />
```

Chúng ta sẽ nhận báo lỗi ở console.log.

Ở bài tập này, bạn hãy thêm `propTypes` và viết 1 function validate dành cho `subject` và `greeting`.

Lưu ý rằng việc sử dụng `propTypes` sẽ gây ảnh hưởng làm chậm performance, nên chúng sẽ không được chạy trên môi trường production.

Tham khảo https://reactjs.org/docs/typechecking-with-proptypes.html

#### ** ĐÁP ÁN 4 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		const PropTypes = {
			string(props, propName, componentName) {
				if (typeof props[propName] !== "string") {
					return new Error(
						`Hey, the component ${componentName} needs the prop ${propName} to be a string, but a ${typeof props[
							propName
						]} was passed`
					);
				}
			},
		};

		function Message({ subject, greeting }) {
			return (
				<div className="message">
					{greeting}, {subject}
				</div>
			);
		}

		Message.propTypes = {
			subject: PropTypes.string,
			greeting: PropTypes.string,
		};

		const element = (
			<div className="container">
				<Message subject="Make" greeting="FE" />
				<Message subject="Great" greeting="Again" />
			</div>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 5 **

### 4. Sử dụng thư viện prop-types

[`prop-types`](https://npm.im/prop-types).

Thêm thẻ script sau vào:

```html
<script src="https://unpkg.com/prop-types@15.7.2/prop-types.js"></script>
```

Sử dụng thư viện thay vì xài function của bạn.

#### ** ĐÁP ÁN 5 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script src="https://unpkg.com/prop-types@15.7.2/prop-types.js"></script>
	<script type="text/babel">
		function Message({ subject, greeting }) {
			return (
				<div className="message">
					{greeting}, {subject}
				</div>
			);
		}
		Message.propTypes = {
			subject: PropTypes.string.isRequired,
			greeting: PropTypes.string.isRequired,
		};

		const element = (
			<div className="container">
				<Message subject="Make" greeting="FE" />
				<Message subject="Great" greeting="Again" />
			</div>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI TẬP 6 **

### Sử dụng React Fragments

["React Fragments"](https://reactjs.org/docs/fragments.html). Là một loại React componenet đặc biệt cho phép bạn đặt 2 components cạnh nhau thay vì lồng vào chung 1 parent componenet.

Cách sử dụng `<React.Fragment>` (hoặc
[cú pháp rút gọn](https://reactjs.org/docs/fragments.html#short-syntax) thẻ mở `<>` và thẻ đóng `</>`). Thay `<div className="container">` với fragment

🐧 Sử dụng `<React.Fragment>` thay vì sử dụng thẻ div container

#### ** ĐÁP ÁN 6 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script src="https://unpkg.com/prop-types@15.7.2/prop-types.js"></script>
	<script type="text/babel">
		function Message({ subject, greeting }) {
			return (
				<div className="message">
					{greeting}, {subject}
				</div>
			);
		}
		Message.propTypes = {
			subject: PropTypes.string.isRequired,
			greeting: PropTypes.string.isRequired,
		};

		const element = (
			<>
				<Message subject="Make" greeting="FE" />
				<Message subject="Great" greeting="Again" />
			</>
		);

		ReactDOM.render(element, document.getElementById("root"));
	</script>
</body>
```

<!-- tabs:end -->

# Form

Trong React, bạn không cần mất nhiều thời gian để học cách tương tác với Form vì nó gần như tương tự các bạn làm với DOM APIs và JavaScript.

Bạn có thể thêm việc sử lý submit đến form element thông qua `onSubmit` prop. Khi đó submit event sẽ được gọi đến một `target`. `target` này tham chiếu đến `<form>` DOM node và lại có tham chiếu để các element của form vì thế có thể dùng để lấy các values của form.

<!-- slide:break -->

<!-- tabs:start -->

#### ** BÀI 1 **

Trong bài này, chúng ta có 1 form dùng để submit `username` và hiển thị lên bằng `alert`.

Có nhiều các để lấy value của name input:

-   Qua index: `event.target.elements[0].value`
-   Qua đối tượng elements bằng `name` hay `id`:
    `event.target.elements.usernameInput.value`
-   Và 1 số các khác trong các bài sau

#### ** ĐÁP ÁN 1 **

```html
<
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		function UsernameForm({ onSubmitUsername }) {
			function handleSubmit(event) {
				event.preventDefault();
				onSubmitUsername(event.target.elements.usernameInput.value);
			}

			return (
				<form onSubmit={handleSubmit}>
					<div>
						<label htmlFor="usernameInput">Username:</label>
						<input id="usernameInput" type="text" />
					</div>
					<button type="submit">Submit</button>
				</form>
			);
		}

		function App() {
			const onSubmitUsername = (username) =>
				alert(`You entered: ${username}`);
			return <UsernameForm onSubmitUsername={onSubmitUsername} />;
		}
		ReactDOM.render(App(), document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI 2 **

### Sử dụng refs

Một cách khác để lấy value là thông qua `ref` trong React. `ref` là một đối tượng luôn nhất quán mỗi khi React Component render.
Nó có prop `current` có thể được update thành bất kỳ giá trị nào và mọi thời điểm.Trong trường hợp tương tác với DOM node, bạn có thể truyền một `ref` đến 1 React element và React sẽ set `current` prop đến DOM node mà nó đã render.

Vì vậy nếu chúng ta tao 1 đối tượng `inputRef` thông qua `React.useRef`, bạn có thể lấy value bằng cách: `inputRef.current.value`

#### ** ĐÁP ÁN 2 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		function UsernameForm({ onSubmitUsername }) {
			const usernameInputRef = React.useRef();

			function handleSubmit(event) {
				event.preventDefault();
				onSubmitUsername(usernameInputRef.current.value);
			}

			return (
				<form onSubmit={handleSubmit}>
					<div>
						<label htmlFor="usernameInput">Username:</label>
						<input
							id="usernameInput"
							type="text"
							ref={usernameInputRef}
						/>
					</div>
					<button type="submit">Submit</button>
				</form>
			);
		}

		function App() {
			const onSubmitUsername = (username) =>
				alert(`You entered: ${username}`);
			return <UsernameForm onSubmitUsername={onSubmitUsername} />;
		}
		ReactDOM.render(App(), document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI 3 **

### Validate chữ in thường

Với React, cách mà chúng ta sử dụng state là thông qua `useState` hook. Ví dụ:

```jsx
function Counter() {
	const [count, setCount] = React.useState(0);
	const increment = () => setCount(count + 1);
	return <button onClick={increment}>{count}</button>;
}
```

`React.useState` nhận vào 1 giá trị default và trả ra một array. Chúng ta sẽ phải destructure array thành state và state updater function.

📜 https://reactjs.org/docs/hooks-state.html

🐧 Trong bài này, chúng ta chỉ cho phép username input chữ in thường. Vì vậy nếu ai đó nhập vào chũ in hoa, thì sẽ không hợp lệ và chúng ta sẽ thông báo lỗi.

Một số điều ta cần làm như sau:

1. State để lưu trữ giá trị động (trường hợp này của chúng ta là thông báo lỗi)
2. Xử lý `onChange` trên input để chúng ta biêt giá trị username thay đổi và cập nhật state error.

Khi username không hợp lệ chúng ta sẽ render thông báo lỗi và disable submit button.

Các bước thực hiện:

1. Tạo 1 hàm `handleChange` function nhận vào change `event` và sử dụng
   `event.target.value` để lấy giá trị của input. Lưu ý rằng event nầy được trigger trên input chứ không phải form.
2. Sử dụng giá trị của input để xác định có lỗi hay không. Xảy ra lỗi nếu user nhập vào có bắt kỳ chữ in hoa nào đó. Bạn có thể kiểm tra bằng cách `const isValid = value === value.toLowerCase()`
3. Nếu có lỗi, set cập nhật error state thành `'Username phải là chữ in thường'`.
   (`setError(isValid ? null : 'Username phải là chữ in thường')`) và disable
   submit button.
4. Cuối cùng hiển thị thông báo lỗi trên element

#### ** ĐÁP ÁN 3 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		function UsernameForm({ onSubmitUsername }) {
			const [error, setError] = React.useState(null);

			function handleSubmit(event) {
				event.preventDefault();
				onSubmitUsername(event.target.elements.usernameInput.value);
			}

			function handleChange(event) {
				const { value } = event.target;
				const isLowerCase = value === value.toLowerCase();
				setError(isLowerCase ? null : "Username phải là chữ in thường");
			}

			return (
				<form onSubmit={handleSubmit}>
					<div>
						<label htmlFor="usernameInput">Username:</label>
						<input
							id="usernameInput"
							type="text"
							onChange={handleChange}
						/>
					</div>
					<div role="alert" style={{ color: "red" }}>
						{error}
					</div>
					<button disabled={Boolean(error)} type="submit">
						Submit
					</button>
				</form>
			);
		}

		function App() {
			const onSubmitUsername = (username) =>
				alert(`You entered: ${username}`);
			return (
				<div style={{ minWidth: 400 }}>
					<UsernameForm onSubmitUsername={onSubmitUsername} />
				</div>
			);
		}
		ReactDOM.render(App(), document.getElementById("root"));
	</script>
</body>
```

#### ** BÀI 4 **

### Controlled component

Đôi khi chúng ta muốn điều khiển hoàn toàn giá trị của Form khi sử dụng. Như là bạn muốn set gía trị cho chúng khi user click một button, hay bạn muốn thay đổi giá mà người dùng nhập vào.

React có hỗ trợ 1 cái gọi là Controlled Form. Như chúng ta đã làm qua các bài tập trước, tất cả form input đều là "uncontrolled" có nghĩa là browser duy trì state của input.

Nếu bạn muốn cập nhật giá trị của input bạn có thể:
`inputNode.value = 'whatever'` như nhìn nó không hay cho lắm. Thay vào đó, React cho phút chúng ta set `value` prop như sau:

```jsx
<input value={myInputValue} />
```

React sẽ giúp chúng ta đảm bảo rằng value của input sẽ không bao giờ khác so với `myInputValue`.

Thường thì chúng ta sẽ cung cấp function `onChange` để xử lý giá trị của input khi có thay đổi.
Khi đó value của input sẽ được lưu trong biến state (thông qua `React.useState`) và khi đó function `onChange` sẽ gọi state updater để cập nhật state.

🐧 Trong bài này thay vì hiển thị thông báo lỗi và cho phép user nhập invalid username. Chúng ta sẽ bỏ thông báo lỗi đi.
Chúng ta sẽ control input và giá trị của nó. Khi input value change chúng ta gọi `.toLowerCase()` để convert và chắc rằng user luôn nhập vào chữ in thường.

Chúng ta thay `error` state bằng `username` (với `React.useState`) và set `username` là giá trị input.
. Khi đó chúng ta truyền `username` vào `input` `value` prop và nó đảm bảo rằng giá trị username luôn nhập vào là hợp lệ!

#### ** ĐÁP ÁN 4 **

```html
<body>
	<div id="root"></div>
	<script src="https://unpkg.com/react@17.0.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@17.0.0/umd/react-dom.development.js"></script>
	<script src="https://unpkg.com/@babel/standalone@7.12.4/babel.js"></script>
	<script type="text/babel">
		function UsernameForm({ onSubmitUsername }) {
			const [username, setUsername] = React.useState("");

			function handleSubmit(event) {
				event.preventDefault();
				onSubmitUsername(username);
			}

			function handleChange(event) {
				setUsername(event.target.value.toLowerCase());
			}

			return (
				<form onSubmit={handleSubmit}>
					<div>
						<label htmlFor="usernameInput">Username:</label>
						<input
							id="usernameInput"
							type="text"
							onChange={handleChange}
							value={username}
						/>
					</div>
					<button type="submit">Submit</button>
				</form>
			);
		}

		function App() {
			const onSubmitUsername = (username) =>
				alert(`You entered: ${username}`);
			return (
				<div style={{ minWidth: 400 }}>
					<UsernameForm onSubmitUsername={onSubmitUsername} />
				</div>
			);
		}
		ReactDOM.render(App(), document.getElementById("root"));
	</script>
</body>
```

<!-- tabs:end -->
